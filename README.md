# Loyalty Management Service
This is my implementation of your homework assignment

First of all, I am really sorry for its size. It got out of control, mainly because I wanted to try out some approaches and techniques for implementing CRUD-ish applications. The whole idea may seem a little bit over engineered, but the fun in doing homework is also important! :)

I focused mainly on API related to Client, you can find endpoints for Client create, update, delete. For other entities, I have just implemented `findAll` and `findById` operations. There are places where I picked an easier way and just used repositories in controllers to save time (although I don't think it's a bad idea when data is only moved in and out of the system).

`Service`s and `Purchase`s seemed so similar that I decided to mix them into a single entity `Action` (although I still differentiate between them)

Hope you like it!

## How to run a program
### Requirements
JDK 11 and internet connection should be enough

### Commands
#### Tests
Tests execute by the way of building, but to just run tests: `./gradlew test`

There are also some integration tests, to run them: `./gradlew integrationTest`

#### Building
* To build a project, just type: `./gradlew build`
* For executable jar: `./gradlew bootJar` (it will end up in `/build/libs/` directory)

#### Running the program
* You can either start it from a jar file: `java -jar loyalty-service-0.0.1-SNAPSHOT.jar`
* Or let gradle do it: `./gradlew bootRun`

## How to use a program
Just hit http://localhost:8080/api with any http client (browser may not be enough), set `Accept` header to `application/json` and play around. Quickly you will discover that there's no much fun without data, so - let's move on to importing

### Importing data

It's easiest to use `curl`:

```
curl -i -X POST localhost:8080/api/import/clients -H "Content-Type: text/csv" --data-binary "@clients.csv"
curl -i -X POST localhost:8080/api/import/appointments -H "Content-Type: text/csv" --data-binary "@appointments.csv"
curl -i -X POST localhost:8080/api/import/services -H "Content-Type: text/csv" --data-binary "@services.csv"
curl -i -X POST localhost:8080/api/import/purchases -H "Content-Type: text/csv" --data-binary "@purchases.csv"
```

But any other http client will do

## Issues
* When inserting CSV data, Hibernate first selects given entity from DB to see if it already exists or not. This has both pluses and minuses - first of all, we get update-by-import out of the box. On the other hand there's a performance penalty for being that noisy. It can be solved by deciding that import should always import new entities and if yes - then implementing `org.springframework.data.domain.Persistable` on entities.
* Inserting can be also speed up by batching inserts, both on CSV Importers and Hibernate side. I just focused on something else.
* For `Appointment`s, I have quickly decided to use `ZonedDateTime` which was not the best idea as it introduces mess and additional complexity - the easiest way would be to convert all datetimes to a single timezone. Now, we have a dependency on a DB - ideally it should have `timestamp with zone` type
* CSV Imports fail when there's an error during parsing - it could just report failed rows and import a good ones as well 
* Too many DTOs - that's right, I just wanted to have a good separation between `domain` and the rest of the world - even Csv Importers get their own structures. Unfortunately, this means a lot of work maintaining it
* There should be also `/api/clients/{id}/appointments` mapping (and similar), but I focused on something else

## Other stuff

#### HATEOAS
As I told you during an interview - I never had a chance to implement HATEOAS in a project larger than few classes. Now I had and to be honest, that's lot of boilerplate code and work, but in fact - it's much easier to navigate through and explore API.

When it comes to HATEOAS - there's always a problem with putting there HTTP verbs like `POST`, `PUT`, `DELETE`. [HATEOAS with Affordances](https://spring.io/blog/2018/01/12/building-richer-hypermedia-with-spring-hateoas) could be a save, but in this very case, it is still a snapshot and support for it so poor (and I didn't want to build something similar manually) that I decided to cover just `GET`s.

#### Gradle and warnings
* `WARNING: An illegal reflective access operation has occurred` - this is related to Groovy and JRE 11 and is not resolved as of the time I write this
* `Some other warning, hidden deep inside` - It looks like one of plugins that I used is not yet ready for Gradle 6.0. I put a `gradle.properties` file to cover that.

#### integrationTest
In `src` directory you will find `integrationTest` - it's a separate source set for integration tests (honestly, more e2e than integration)