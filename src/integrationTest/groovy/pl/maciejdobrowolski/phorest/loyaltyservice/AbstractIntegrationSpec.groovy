package pl.maciejdobrowolski.phorest.loyaltyservice

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@AutoConfigureMockMvc
@SpringBootTest
abstract class AbstractIntegrationSpec extends Specification {

    @Autowired
    protected ObjectMapper objectMapper

    protected String toJson(Object o) {
        return objectMapper.writeValueAsString(o)
    }

}
