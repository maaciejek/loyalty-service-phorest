package pl.maciejdobrowolski.phorest.loyaltyservice


import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ActionType
import pl.maciejdobrowolski.phorest.loyaltyservice.importing.ActionCsvImporter
import pl.maciejdobrowolski.phorest.loyaltyservice.importing.AppointmentCsvImporter
import pl.maciejdobrowolski.phorest.loyaltyservice.importing.ClientCsvImporter

import javax.annotation.PostConstruct

class RankingFixture {

    private final ActionCsvImporter actionImporter
    private final ClientCsvImporter clientImporter
    private final AppointmentCsvImporter appointmentImporter

    @Value('classpath:ranking_fixture/clients.csv')
    Resource clients

    @Value('classpath:ranking_fixture/appointments.csv')
    Resource appointments

    @Value('classpath:ranking_fixture/services.csv')
    Resource services

    @Value('classpath:ranking_fixture/purchases.csv')
    Resource purchases

    RankingFixture(ActionCsvImporter actionImporter,
                   ClientCsvImporter clientImporter,
                   AppointmentCsvImporter appointmentImporter) {
        this.actionImporter = actionImporter
        this.clientImporter = clientImporter
        this.appointmentImporter = appointmentImporter
    }

    @PostConstruct
    void init() {
        clients.getFile().withInputStream { clientImporter.importClients(it) }
        appointments.getFile().withInputStream { appointmentImporter.importAppointments(it) }
        services.getFile().withInputStream { actionImporter.importActions(it, ActionType.SERVICE) }
        purchases.getFile().withInputStream { actionImporter.importActions(it, ActionType.PURCHASE) }
    }

}
