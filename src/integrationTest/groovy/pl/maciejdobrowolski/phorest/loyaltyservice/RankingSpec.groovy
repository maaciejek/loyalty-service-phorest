package pl.maciejdobrowolski.phorest.loyaltyservice

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import pl.maciejdobrowolski.phorest.loyaltyservice.importing.ActionCsvImporter
import pl.maciejdobrowolski.phorest.loyaltyservice.importing.AppointmentCsvImporter
import pl.maciejdobrowolski.phorest.loyaltyservice.importing.ClientCsvImporter

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

class RankingSpec extends AbstractIntegrationSpec {

    @Autowired
    MockMvc mockMvc

    def 'should return expected ranking for fixture'() {
        expect:
            mockMvc.perform(get("/api/ranking/top10").param('since', '2010-01-01')
                    .accept(MediaType.APPLICATION_JSON_UTF8)
            )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath('$.ranking.length()').value(10))
                    .andExpect(jsonPath('$.ranking[0].firstName').value('Christen'))
                    .andExpect(jsonPath('$.ranking[0].lastName').value('Hermann'))
                    .andExpect(jsonPath('$.ranking[0].totalPoints').value(965))
                    .andExpect(jsonPath('$.ranking[1].firstName').value('Roxie'))
                    .andExpect(jsonPath('$.ranking[1].lastName').value('Rau'))
                    .andExpect(jsonPath('$.ranking[1].totalPoints').value(945))
    }

    @TestConfiguration
    static class FixtureConfiguration {
        @Bean
        RankingFixture rankingFixture(ActionCsvImporter actionImporter,
                                      ClientCsvImporter clientImporter,
                                      AppointmentCsvImporter appointmentImporter) {
            return new RankingFixture(actionImporter, clientImporter, appointmentImporter)
        }
    }

}