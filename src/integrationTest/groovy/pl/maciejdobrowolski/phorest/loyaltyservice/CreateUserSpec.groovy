package pl.maciejdobrowolski.phorest.loyaltyservice

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Gender
import pl.maciejdobrowolski.phorest.loyaltyservice.rest.client.CreateClientRequest
import spock.lang.Unroll

import java.nio.charset.StandardCharsets

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

class CreateUserSpec extends AbstractIntegrationSpec {

    @Autowired
    MockMvc mockMvc

    def 'should create a new user'() {
        given:
            CreateClientRequest createClientRequest = new CreateClientRequest([
                    firstName: 'Andy',
                    lastName : 'Hoe',
                    email    : 'andy@andy.com',
                    phone    : '22123412',
                    gender   : Gender.M,
                    banned   : false
            ])
        expect:
            MvcResult creationResult = mockMvc.perform(post('/api/clients')
                    .accept(MediaType.APPLICATION_JSON_UTF8)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .characterEncoding(StandardCharsets.UTF_8.name())
                    .content(toJson(createClientRequest))
            )
                    .andExpect(status().isCreated())
                    .andExpect(header().exists(HttpHeaders.LOCATION))
                    .andReturn()
        and:
            String location = creationResult.response.getHeader(HttpHeaders.LOCATION)
            mockMvc.perform(get(location)
                    .accept(MediaType.APPLICATION_JSON_UTF8)
            )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath('$.firstName').value('Andy'))
                    .andExpect(jsonPath('$.lastName').value('Hoe'))
                    .andExpect(jsonPath('$.email').value('andy@andy.com'))
                    .andExpect(jsonPath('$.phone').value('22123412'))
                    .andExpect(jsonPath('$.gender').value('M'))
                    .andExpect(jsonPath('$.banned').value(false))
    }

    @Unroll
    def 'should not allow to create a new user if request is invalid (invalid #whatsWrong)'() {
        given:
            CreateClientRequest createClientRequest = new CreateClientRequest([
                    firstName: firstName,
                    lastName : lastName,
                    email    : email,
                    phone    : phone,
                    gender   : gender,
                    banned   : false
            ])
        expect:
            mockMvc.perform(post('/api/clients')
                    .accept(MediaType.APPLICATION_JSON_UTF8)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .characterEncoding(StandardCharsets.UTF_8.name())
                    .content(toJson(createClientRequest))
            )
                    .andExpect(status().isBadRequest())
                    .andExpect(header().doesNotExist(HttpHeaders.LOCATION))
                    .andReturn()
        where:
            firstName | lastName | email           | phone     | gender || whatsWrong
            null      | 'Hoe'    | 'andy@andy.com' | '2202332' | 'M'    || 'firstName'
            'Andy'    | null     | 'andy@andy.com' | '2202332' | 'M'    || 'lastName'
            'Andy'    | 'Hoe'    | null            | '2202332' | 'M'    || 'email - null'
            'Andy'    | 'Hoe'    | 'WRONG'         | '2202332' | 'M'    || 'email'
            'Andy'    | 'Hoe'    | 'andy@andy.com' | null      | 'M'    || 'phone - null'
            'Andy'    | 'Hoe'    | 'andy@andy.com' | 'WRONG'   | 'M'    || 'phone'
            'Andy'    | 'Hoe'    | 'andy@andy.com' | '2202332' | null   || 'gender'
    }


}