package pl.maciejdobrowolski.phorest.loyaltyservice

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import pl.maciejdobrowolski.phorest.loyaltyservice.business.ClientService
import pl.maciejdobrowolski.phorest.loyaltyservice.business.command.ClientCreateCommand
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ClientId
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Email
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Gender
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.PhoneNumber
import pl.maciejdobrowolski.phorest.loyaltyservice.rest.client.UpdateClientRequest
import spock.lang.Unroll

import java.nio.charset.StandardCharsets

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

class UpdateUserSpec extends AbstractIntegrationSpec {

    static String ORIGINAL_FIRST_NAME = 'Joanne'
    static String ORIGINAL_LAST_NAME = 'Hang'
    static Email ORIGINAL_EMAIL = Email.from('j@h.com')
    static PhoneNumber ORIGINAL_PHONE_NUMBER = PhoneNumber.from('123654789')
    static Gender ORIGINAL_GENDER = Gender.F
    static boolean ORIGINAL_BANNED = false

    @Autowired
    MockMvc mockMvc

    @Autowired
    ClientService clientService

    ClientId clientId

    void setup() {
        clientId = clientService.create(ClientCreateCommand.builder()
                .firstName(ORIGINAL_FIRST_NAME)
                .lastName(ORIGINAL_LAST_NAME)
                .email(ORIGINAL_EMAIL)
                .phone(ORIGINAL_PHONE_NUMBER)
                .gender(ORIGINAL_GENDER)
                .banned(ORIGINAL_BANNED)
                .build()
        )
    }

    @Unroll
    def 'should update #field for given user'() {
        given:
            UpdateClientRequest updateClientRequest = new UpdateClientRequest([
                    firstName: firstName,
                    lastName : lastName,
                    email    : email,
                    phone    : phone,
                    gender   : gender,
                    banned   : banned
            ])
        expect:
            mockMvc.perform(put('/api/clients/{:id}', clientId.asString())
                    .accept(MediaType.APPLICATION_JSON_UTF8)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .characterEncoding(StandardCharsets.UTF_8.name())
                    .content(toJson(updateClientRequest))
            )
                    .andExpect(status().isNoContent())
        and:
            mockMvc.perform(get("/api/clients/{:id}", clientId.asString())
                    .accept(MediaType.APPLICATION_JSON_UTF8)
            )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath('$.firstName').value(firstName))
                    .andExpect(jsonPath('$.lastName').value(lastName))
                    .andExpect(jsonPath('$.email').value(email))
                    .andExpect(jsonPath('$.phone').value(phone))
                    .andExpect(jsonPath('$.gender').value(gender))
                    .andExpect(jsonPath('$.banned').value(banned))
        where:
            firstName | lastName | email        | phone       | gender | banned || field
            'Joanna'  | 'Hang'   | 'j@h.com'    | '123654789' | 'F'    | false  || 'firstName'
            'Joanne'  | 'Fo'     | 'j@h.com'    | '123654789' | 'F'    | false  || 'lastName'
            'Joanne'  | 'Hang'   | 'jh90@h.com' | '123654789' | 'F'    | false  || 'email'
            'Joanne'  | 'Hang'   | 'j@h.com'    | '987654321' | 'F'    | false  || 'phone'
            'Joanne'  | 'Hang'   | 'j@h.com'    | '123654789' | 'M'    | false  || 'gender'
            'Joanne'  | 'Hang'   | 'j@h.com'    | '123654789' | 'F'    | true   || 'banned'
    }

    @Unroll
    def 'should not update given user due to invalid #field field'() {
        given:
            UpdateClientRequest updateClientRequest = new UpdateClientRequest([
                    firstName: firstName,
                    lastName : lastName,
                    email    : email,
                    phone    : phone,
                    gender   : gender,
                    banned   : banned
            ])
        expect:
            mockMvc.perform(put('/api/clients/{:id}', clientId.asString())
                    .accept(MediaType.APPLICATION_JSON_UTF8)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .characterEncoding(StandardCharsets.UTF_8.name())
                    .content(toJson(updateClientRequest))
            )
                    .andExpect(status().isBadRequest())
        and:
            mockMvc.perform(get("/api/clients/{:id}", clientId.asString())
                    .accept(MediaType.APPLICATION_JSON_UTF8)
            )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath('$.firstName').value(ORIGINAL_FIRST_NAME))
                    .andExpect(jsonPath('$.lastName').value(ORIGINAL_LAST_NAME))
                    .andExpect(jsonPath('$.email').value(ORIGINAL_EMAIL.asString()))
                    .andExpect(jsonPath('$.phone').value(ORIGINAL_PHONE_NUMBER.asString()))
                    .andExpect(jsonPath('$.gender').value(ORIGINAL_GENDER.name()))
                    .andExpect(jsonPath('$.banned').value(ORIGINAL_BANNED))
        where:
            firstName | lastName | email        | phone       | gender | banned || field
            ''        | 'Hang'   | 'j@h.com'    | '123654789' | 'F'    | false  || 'firstName'
            'Joanne'  | ''       | 'j@h.com'    | '123654789' | 'F'    | false  || 'lastName'
            'Joanne'  | 'Hang'   | 'root.h.com' | '123654789' | 'F'    | false  || 'email'
            'Joanne'  | 'Hang'   | 'j@h.com'    | '21'        | 'F'    | false  || 'phone'
            'Joanne'  | 'Hang'   | 'j@h.com'    | '123654789' | null   | false  || 'gender'
    }


}