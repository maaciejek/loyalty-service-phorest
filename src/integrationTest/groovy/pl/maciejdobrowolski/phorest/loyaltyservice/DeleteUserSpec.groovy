package pl.maciejdobrowolski.phorest.loyaltyservice

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import pl.maciejdobrowolski.phorest.loyaltyservice.business.ClientService
import pl.maciejdobrowolski.phorest.loyaltyservice.business.command.ClientCreateCommand
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.*
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.AppointmentRepository

import java.time.ZonedDateTime

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class DeleteUserSpec extends AbstractIntegrationSpec {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ClientService clientService

    @Autowired
    AppointmentRepository appointmentRepository;

    ClientId clientId

    void setup() {
        clientId = clientService.create(ClientCreateCommand.builder()
                .firstName('Joanne')
                .lastName('Hang')
                .email(Email.from('j@h.com'))
                .phone(PhoneNumber.from('123654789'))
                .gender(Gender.F)
                .banned(false)
                .build()
        )
    }

    def 'should delete user'() {
        expect:
            mockMvc.perform(delete('/api/clients/{:id}', clientId.asString())
                    .accept(MediaType.APPLICATION_JSON_UTF8)
            )
                    .andExpect(status().isNoContent())
        and:
            mockMvc.perform(get('/api/clients/{:id}', clientId.asString())
                    .accept(MediaType.APPLICATION_JSON_UTF8)
            )
                    .andExpect(status().isNotFound())
    }

    def 'should fail to delete if client has any appointments'() {
        given:
            ZonedDateTime now = ZonedDateTime.now()
            appointmentRepository.save(new Appointment(AppointmentId.random(), clientId, now, now.plusMinutes(30)))
        expect:
            mockMvc.perform(delete('/api/clients/{:id}', clientId.asString())
                    .accept(MediaType.APPLICATION_JSON_UTF8)
            )
                    .andExpect(status().isConflict())
    }


}