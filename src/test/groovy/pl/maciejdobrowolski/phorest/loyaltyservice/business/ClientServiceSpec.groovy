package pl.maciejdobrowolski.phorest.loyaltyservice.business

import pl.maciejdobrowolski.phorest.loyaltyservice.business.command.ClientCreateCommand
import pl.maciejdobrowolski.phorest.loyaltyservice.business.command.ClientUpdateCommand
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.*
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.ClientRepository
import spock.lang.Specification
import spock.lang.Subject

class ClientServiceSpec extends Specification {

    @Subject
    ClientService clientService

    ClientRepository repository = Mock()

    void setup() {
        clientService = new ClientService(repository)
    }

    def 'should throw exception when updating non-existing client'() {
        given:
            repository.findById(_ as ClientId) >> Optional.empty()
        when:
            clientService.update(ClientUpdateCommand.builder()
                    .id(ClientId.from('unknown'))
                    .firstName('Janusz')
                    .lastName('Kowalski')
                    .build()
            )
        then:
            thrown(ClientNotFoundException)
    }

    def 'should update existing client'() {
        given:
            ClientId id = ClientId.from('abc')
            Client client = new Client(
                    id,
                    'Adam',
                    'Mickiewicz',
                    Email.from('a@m.pl'),
                    PhoneNumber.from('227283333'),
                    Gender.M,
                    false
            )
            repository.findById(id) >> Optional.of(client)
        when:
            clientService.update(ClientUpdateCommand.builder()
                    .id(id)
                    .firstName('Alicja')
                    .lastName('Kowalska')
                    .email(Email.from('a@k.pl'))
                    .phone(PhoneNumber.from('321456987'))
                    .gender(Gender.F)
                    .banned(true)
                    .build())
        then:
            client.firstName == 'Alicja'
            client.lastName == 'Kowalska'
            client.email == Email.from('a@k.pl')
            client.phone == PhoneNumber.from('321456987')
            client.gender == Gender.F
            client.banned
    }

    def 'should create client with new id'() {
        when:
            ClientId id = clientService.create(ClientCreateCommand.builder()
                    .firstName('Alicja')
                    .lastName('Kowalska')
                    .email(Email.from('a@k.pl'))
                    .phone(PhoneNumber.from('321456987'))
                    .gender(Gender.F)
                    .build())
        then:
            id != null
        and:
            1 * repository.save({ Client client -> client.id != null } as Client) >> { Client client -> client }
    }

    def 'should create client with provided id'() {
        given:
            ClientId providedId = ClientId.from('abc')
        when:
            ClientId id = clientService.create(ClientCreateCommand.builder()
                    .id(providedId)
                    .firstName('Alicja')
                    .lastName('Kowalska')
                    .email(Email.from('a@k.pl'))
                    .phone(PhoneNumber.from('321456987'))
                    .gender(Gender.F)
                    .build())
        then:
            id == providedId
        and:
            1 * repository.save({ Client client -> client.id == providedId } as Client) >> { Client client -> client }
    }


}
