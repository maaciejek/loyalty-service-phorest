package pl.maciejdobrowolski.phorest.loyaltyservice.domain

import spock.lang.Specification
import spock.lang.Unroll

class EmailSpec extends Specification {

    @Unroll
    def 'should throw exception when email (#email) does not contain @'() {
        when:
            Email.from(email)
        then:
            thrown(InvalidEmailAddressException)
        where:
            email << [
                    'just letters and white space',
                    'some.pl',
                    '             ',
                    'root.com.tw'
            ]
    }

    @Unroll
    def 'should create instance if email (#email) contains @ or is null'() {
        when:
            Email.from(email)
        then:
            noExceptionThrown()
        where:
            email << [
                    '@', // great example :)
                    'jacek@some.pl',
                    '     ola@wp.pl        ',
                    'root@com.tw',
                    null
            ]
    }

}
