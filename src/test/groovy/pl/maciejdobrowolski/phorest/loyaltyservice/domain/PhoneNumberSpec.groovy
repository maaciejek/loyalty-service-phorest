package pl.maciejdobrowolski.phorest.loyaltyservice.domain

import spock.lang.Specification
import spock.lang.Unroll

class PhoneNumberSpec extends Specification {

    @Unroll
    def 'should throw exception when phone number (#number) is shorter than 3 digits '() {
        when:
            PhoneNumber.from(number)
        then:
            thrown(InvalidPhoneNumberException)
        where:
            number << [
                    'just letters and white space',
                    'some letters and one digit 1',
                    '             ',
                    '2'
            ]
    }

    @Unroll
    def 'should create instance if phone number (#number) has at least three digits'() {
        when:
            PhoneNumber.from(number)
        then:
            noExceptionThrown()
        where:
            number << [
                    '+227283113',
                    '233',
                    '   2    3    2  ',
            ]
    }

    @Unroll
    def 'created phone number instance should be sanitized (#number -> #expectedForm)'() {
        when:
            PhoneNumber phoneNumber = PhoneNumber.from(number)
        then:
            phoneNumber.asString() == expectedForm
        where:
            number        || expectedForm
            ' 2 1 2   '   || '212'
            '+2283291102' || '2283291102'
            '080ss9920'   || '0809920' // also not that great example, but those are my imagined business requirements!
    }

}
