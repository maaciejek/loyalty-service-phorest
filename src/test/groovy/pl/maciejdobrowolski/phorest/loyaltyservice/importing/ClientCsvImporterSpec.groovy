package pl.maciejdobrowolski.phorest.loyaltyservice.importing

import pl.maciejdobrowolski.phorest.loyaltyservice.business.ClientService
import pl.maciejdobrowolski.phorest.loyaltyservice.business.command.ClientCreateCommand
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ClientId
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Email
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Gender
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.PhoneNumber
import spock.lang.Specification
import spock.lang.Subject

import static pl.maciejdobrowolski.phorest.loyaltyservice.importing.ClientCsvImporter.CsvClient
import static pl.maciejdobrowolski.phorest.loyaltyservice.importing.ClientCsvImporter.CsvGender

class ClientCsvImporterSpec extends Specification {

    ClientService service = Mock()

    FakeCsvReader reader = new FakeCsvReader()

    @Subject
    ClientCsvImporter importer = new ClientCsvImporter(service, reader)

    def 'should map all csv columns'() {
        given:
            InputStream is = GroovyMock(InputStream)
            CsvClient csvClient = new CsvClient([
                    id       : '1',
                    firstName: 'Joe',
                    lastName : 'Dutch',
                    email    : 'j@d.com',
                    phone    : '20210',
                    gender   : CsvGender.Male,
                    banned   : true

            ])
            reader.results = [csvClient]
        and:
            ClientCreateCommand expectedCommand = ClientCreateCommand.builder()
                    .id(ClientId.from(csvClient.id))
                    .firstName(csvClient.firstName)
                    .lastName(csvClient.lastName)
                    .email(Email.from(csvClient.email))
                    .phone(PhoneNumber.from(csvClient.phone))
                    .gender(Gender.M)
                    .banned(true)
                    .build()
        when:
            importer.importClients(is)
        then:
            1 * service.create(expectedCommand)
    }
}
