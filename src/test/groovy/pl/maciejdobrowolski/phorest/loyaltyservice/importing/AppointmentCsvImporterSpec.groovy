package pl.maciejdobrowolski.phorest.loyaltyservice.importing


import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Appointment
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.AppointmentRepository
import spock.lang.Specification
import spock.lang.Subject

import java.time.ZonedDateTime

import static pl.maciejdobrowolski.phorest.loyaltyservice.importing.AppointmentCsvImporter.CsvAppointment

class AppointmentCsvImporterSpec extends Specification {

    AppointmentRepository repository = Mock()

    FakeCsvReader reader = new FakeCsvReader()

    @Subject
    AppointmentCsvImporter importer = new AppointmentCsvImporter(repository, reader)

    def 'should map all csv columns'() {
        given:
            InputStream is = GroovyMock(InputStream)
            ZonedDateTime now = ZonedDateTime.now()
            CsvAppointment csvAppointment = new CsvAppointment([
                    id      : 'sample-app',
                    clientId: 'sample-client',
                    start   : now,
                    end     : now.plusMinutes(10)
            ])
            reader.results = [csvAppointment]
        when:
            importer.importAppointments(is)
        then:
            1 * repository.save({ Appointment appointment ->
                appointment.id.asString() == csvAppointment.id &&
                        appointment.id.asString() == csvAppointment.id &&
                        appointment.clientId.asString() == csvAppointment.clientId &&
                        appointment.start == csvAppointment.start &&
                        appointment.end == csvAppointment.end
            } as Appointment)
    }
}
