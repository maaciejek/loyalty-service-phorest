package pl.maciejdobrowolski.phorest.loyaltyservice.importing

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.csv.CsvMapper
import spock.lang.Specification

import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class JacksonCsvReaderSpec extends Specification {

    static String csv = """id,price,height,bool,datetime
2fa14404-890a-4b83-9088-6fc236d3934f,10.10,150,true,2018-11-22 10:15:00 +0000
f543b646-52b9-4c8e-a80c-4573a83b0068,20.30,200,false,2018-12-04 15:00:00 +0000
"""

    JacksonCsvReader csvReader = new JacksonCsvReader(new CsvMapper().findAndRegisterModules() as CsvMapper)

    def 'should read test file'() {
        given:
            List<CsvTestItem> results = []
        when:
            csvReader.read(new ByteArrayInputStream(csv.bytes), CsvTestItem, { results.add(it) })
        then:
            results.size() == 2
        and:
            results.any { CsvTestItem item ->
                item.id == '2fa14404-890a-4b83-9088-6fc236d3934f' &&
                        item.price == 10.10 &&
                        item.height == 150 &&
                        item.bool &&
                        item.dateTime == zonedDateTime('2018-11-22 10:15:00 +0000')
            }
        and:
            results.any { CsvTestItem item ->
                item.id == 'f543b646-52b9-4c8e-a80c-4573a83b0068' &&
                        item.price == 20.30 &&
                        item.height == 200 &&
                        !item.bool &&
                        item.dateTime == zonedDateTime('2018-12-04 15:00:00 +0000')
            }
    }

    private static ZonedDateTime zonedDateTime(String datetime) {
        return ZonedDateTime.parse(datetime, DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss Z'))
                .withZoneSameInstant(ZoneId.of('UTC'))
    }

    static class CsvTestItem {

        @JsonProperty("id")
        String id

        @JsonProperty("price")
        BigDecimal price

        @JsonProperty("height")
        long height

        @JsonProperty("bool")
        boolean bool

        @JsonProperty("datetime")
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss Z")
        ZonedDateTime dateTime
    }

}
