package pl.maciejdobrowolski.phorest.loyaltyservice.importing

import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Action
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ActionType
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.ActionRepository
import spock.lang.Specification
import spock.lang.Subject

import static pl.maciejdobrowolski.phorest.loyaltyservice.importing.ActionCsvImporter.CsvAction

class ActionCsvImporterSpec extends Specification {

    ActionRepository repository = Mock()

    FakeCsvReader reader = new FakeCsvReader()

    @Subject
    ActionCsvImporter importer = new ActionCsvImporter(repository, reader)

    def 'should map all csv columns'() {
        given:
            InputStream is = GroovyMock(InputStream)
            CsvAction csvAction = new CsvAction([
                    id           : '1',
                    appointmentId: '2',
                    name         : 'name',
                    price        : 123.00,
                    loyaltyPoints: 20

            ])
            reader.results = [csvAction]
        when:
            importer.importActions(is, ActionType.PURCHASE)
        then:
            1 * repository.save({ Action action ->
                action.id.asString() == csvAction.id &&
                        action.appointmentId.asString() == csvAction.appointmentId &&
                        action.name == csvAction.name &&
                        action.price == csvAction.price &&
                        action.loyaltyPoints == csvAction.loyaltyPoints &&
                        action.type == ActionType.PURCHASE
            } as Action)
    }
}
