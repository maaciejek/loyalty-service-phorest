package pl.maciejdobrowolski.phorest.loyaltyservice.importing

class FakeCsvReader<E> implements CsvReader {

    Collection<E> results

    @Override
    <T> void read(InputStream inputStream, Class<T> rowClass, JacksonCsvReader.Callback<T> rowCallback) {
        results.each { rowCallback.process(it as T) }
    }
}
