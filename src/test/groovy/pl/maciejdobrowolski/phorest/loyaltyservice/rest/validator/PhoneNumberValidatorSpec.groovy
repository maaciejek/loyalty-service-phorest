package pl.maciejdobrowolski.phorest.loyaltyservice.rest.validator


import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import javax.validation.ConstraintValidatorContext

class PhoneNumberValidatorSpec extends Specification {

    @Subject
    PhoneNumberValidator validator = new PhoneNumberValidator()

    @Unroll
    def 'should mark phone number (#number) as invalid'() {
        expect:
            !validator.isValid(number, Stub(ConstraintValidatorContext))
        where:
            number << [
                    'just letters and white space',
                    'some letters and one digit 1',
                    '             ',
                    '2'
            ]
    }

    @Unroll
    def 'should mark phone number (#number) as valid'() {
        expect:
            validator.isValid(number, Stub(ConstraintValidatorContext))
        where:
            number << [
                    '+227283113',
                    '233',
                    '   2    3    2  ',
            ]
    }
}
