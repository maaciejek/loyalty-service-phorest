package pl.maciejdobrowolski.phorest.loyaltyservice.rest.client;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Gender;

@Data
@Relation(value = "client", collectionRelation = "clients")
@EqualsAndHashCode(callSuper = false)
public class ClientResource extends ResourceSupport {

    String firstName;
    String lastName;
    String email;
    String phone;
    Gender gender;
    boolean banned;

}
