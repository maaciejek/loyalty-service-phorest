package pl.maciejdobrowolski.phorest.loyaltyservice.rest.imports;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ActionType;
import pl.maciejdobrowolski.phorest.loyaltyservice.importing.ActionCsvImporter;

import java.io.IOException;

@RestController
@RequestMapping(path = "/api/import")
@Slf4j
public class ActionImportController {

    private final ActionCsvImporter csvImporter;

    public ActionImportController(ActionCsvImporter csvImporter) {
        this.csvImporter = csvImporter;
    }

    @PostMapping(path = "purchases", consumes = {CsvMediaTypes.APP_CSV, CsvMediaTypes.TEXT_CSV})
    public ResponseEntity<?> importPurchases(@RequestBody Resource stream) {
        return importAction(stream, ActionType.PURCHASE);
    }

    @PostMapping(path = "services", consumes = {CsvMediaTypes.APP_CSV, CsvMediaTypes.TEXT_CSV})
    public ResponseEntity<?> importServices(@RequestBody Resource stream) {
        return importAction(stream, ActionType.SERVICE);
    }

    private ResponseEntity<?> importAction(Resource stream, ActionType purchase) {
        try {
            csvImporter.importActions(stream.getInputStream(), purchase);
            return ResponseEntity.noContent().build();
        } catch (IOException e) {
            log.error("There is a problem with reading csv input stream", e);
            return ResponseEntity.badRequest().build();
        }
    }
}
