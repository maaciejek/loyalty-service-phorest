package pl.maciejdobrowolski.phorest.loyaltyservice.rest.client;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Gender;
import pl.maciejdobrowolski.phorest.loyaltyservice.rest.validator.ValidPhoneNumber;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
public class CreateClientRequest {

    @Null
    String id;

    @NotBlank
    @Length(max = 255)
    String firstName;

    @NotBlank
    @Length(max = 255)
    String lastName;

    @NotBlank
    @Email
    @Length(max = 255)
    String email;

    @NotBlank
    @ValidPhoneNumber
    @Length(max = 20)
    String phone;

    @NotNull
    Gender gender;

    boolean banned;

}
