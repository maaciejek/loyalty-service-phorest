package pl.maciejdobrowolski.phorest.loyaltyservice.rest.ranking;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.ClientWithPointsRepository;
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.projection.ClientWithPoints;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "/api/ranking")
public class RankingController {

    private static final int MIN_RANKING_LENGTH = 1;
    private static final int MAX_RANKING_LENGTH = 1_000;

    private final ClientWithPointsRepository repository;
    private final ClientWithPointsResourceAssembler resourceAssembler;

    public RankingController(ClientWithPointsRepository repository,
                             ClientWithPointsResourceAssembler resourceAssembler) {
        this.repository = repository;
        this.resourceAssembler = resourceAssembler;
    }

    @GetMapping
    public ResourceSupport rankingRoot() {
        ResourceSupport resource = new ResourceSupport();
        LocalDate sinceMonthBeginning = LocalDate.now().withDayOfMonth(1);
        resource.add(linkTo(methodOn(RankingController.class).findTop(10, sinceMonthBeginning)).withRel("top10"));
        resource.add(linkTo(methodOn(RankingController.class).findTop(25, sinceMonthBeginning)).withRel("top25"));
        resource.add(linkTo(methodOn(RankingController.class).findTop(50, sinceMonthBeginning)).withRel("top50"));
        resource.add(linkTo(methodOn(RankingController.class).findTop(100, sinceMonthBeginning)).withRel("top100"));
        return resource;
    }

    @GetMapping("/top{topCount}")
    public ResponseEntity<Ranking> findTop(@PathVariable("topCount") int topCount,
                                           @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam("since") LocalDate since) {
        if (topCount < MIN_RANKING_LENGTH || topCount > MAX_RANKING_LENGTH) {
            return ResponseEntity.badRequest().build();
        }
        ZonedDateTime zonedSince = toSystemDefaultZoned(since);
        List<ClientWithPoints> rankedClients = repository.top(topCount, zonedSince);
        Ranking ranking = new Ranking(zonedSince, resourceAssembler.toResources(rankedClients));
        return ResponseEntity.ok(ranking);
    }

    private ZonedDateTime toSystemDefaultZoned(LocalDate since) {
        LocalDateTime startOfDay = since.atStartOfDay();
        // assuming a server has a proper timezone set
        return startOfDay.atZone(ZoneId.systemDefault());
    }

    @Value
    static class Ranking {
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss Z")
        ZonedDateTime since;
        List<ClientWithPointsResource> ranking;
    }

}
