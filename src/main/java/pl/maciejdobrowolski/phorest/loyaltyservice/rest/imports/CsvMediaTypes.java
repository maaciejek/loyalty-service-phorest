package pl.maciejdobrowolski.phorest.loyaltyservice.rest.imports;

class CsvMediaTypes {

    static final String TEXT_CSV = "text/csv";
    static final String APP_CSV = "application/csv";

}
