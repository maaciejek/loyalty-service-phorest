package pl.maciejdobrowolski.phorest.loyaltyservice.rest.appointment;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Appointment;
import pl.maciejdobrowolski.phorest.loyaltyservice.rest.client.ClientController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class AppointmentResourceAssembler extends ResourceAssemblerSupport<Appointment, AppointmentResource> {

    public AppointmentResourceAssembler() {
        super(AppointmentController.class, AppointmentResource.class);
    }

    @Override
    public AppointmentResource toResource(Appointment appointment) {
        AppointmentResource appointmentResource = createResourceWithId(appointment.getId().asString(), appointment);
        appointmentResource.setEnd(appointment.getEnd());
        appointmentResource.setStart(appointment.getStart());
        appointmentResource.add(linkTo(methodOn(ClientController.class).find(appointment.getClientId().asString())).withRel("client"));
        return appointmentResource;
    }


}
