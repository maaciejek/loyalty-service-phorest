package pl.maciejdobrowolski.phorest.loyaltyservice.rest.action;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ActionType;

import java.math.BigDecimal;

@Data
@Relation(value = "appointment", collectionRelation = "appointments")
@EqualsAndHashCode(callSuper = false)
public class ActionResource extends ResourceSupport {
    String name;
    BigDecimal price;
    long loyaltyPoints;
    ActionType type;
}
