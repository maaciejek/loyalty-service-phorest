package pl.maciejdobrowolski.phorest.loyaltyservice.rest.appointment;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import java.time.ZonedDateTime;

@Data
@Relation(value = "appointment", collectionRelation = "appointments")
@EqualsAndHashCode(callSuper = false)
public class AppointmentResource extends ResourceSupport {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss Z")
    ZonedDateTime start;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss Z")
    ZonedDateTime end;
}
