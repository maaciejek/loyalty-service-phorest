package pl.maciejdobrowolski.phorest.loyaltyservice.rest.client;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.maciejdobrowolski.phorest.loyaltyservice.business.ClientService;
import pl.maciejdobrowolski.phorest.loyaltyservice.business.command.ClientCreateCommand;
import pl.maciejdobrowolski.phorest.loyaltyservice.business.command.ClientUpdateCommand;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Client;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ClientId;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Email;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.PhoneNumber;

import javax.validation.Valid;
import java.net.URI;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "/api/clients")
public class ClientController {

    private final ClientService clientService;
    private final ClientResourceAssembler clientResourceAssembler;
    private final PagedResourcesAssembler<Client> pagedResourcesAssembler;

    public ClientController(ClientService clientService,
                            ClientResourceAssembler clientResourceAssembler,
                            PagedResourcesAssembler<Client> pagedResourcesAssembler) {
        this.clientService = clientService;
        this.clientResourceAssembler = clientResourceAssembler;
        this.pagedResourcesAssembler = pagedResourcesAssembler;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientResource> find(@PathVariable("id") String clientId) {
        return clientService.findById(ClientId.from(clientId))
                .map(clientResourceAssembler::toResource)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable("id") String clientId, @Valid @RequestBody UpdateClientRequest updateRequest) {
        clientService.update(
                ClientUpdateCommand.builder()
                        .id(ClientId.from(clientId))
                        .banned(updateRequest.isBanned())
                        .email(Email.from(updateRequest.getEmail()))
                        .firstName(updateRequest.getFirstName())
                        .lastName(updateRequest.getLastName())
                        .gender(updateRequest.getGender())
                        .phone(PhoneNumber.from(updateRequest.getPhone()))
                        .build()
        );
        return ResponseEntity.noContent().build();
    }

    @PostMapping
    public ResponseEntity<?> add(@Valid @RequestBody CreateClientRequest addClientRequest) {
        ClientId clientId = clientService.create(
                ClientCreateCommand.builder()
                        .banned(addClientRequest.isBanned())
                        .email(Email.from(addClientRequest.getEmail()))
                        .firstName(addClientRequest.getFirstName())
                        .lastName(addClientRequest.getLastName())
                        .gender(addClientRequest.getGender())
                        .phone(PhoneNumber.from(addClientRequest.getPhone()))
                        .build()
        );

        URI newClientUri = linkTo(methodOn(ClientController.class).find(clientId.asString())).toUri();
        return ResponseEntity.created(newClientUri).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") String clientId) {
        clientService.delete(ClientId.from(clientId));
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public Resources<ClientResource> findAll(@PageableDefault(size = 20) Pageable pageable) {
        Page<Client> clients = clientService.findAll(pageable);
        return pagedResourcesAssembler.toResource(clients, clientResourceAssembler);
    }

}
