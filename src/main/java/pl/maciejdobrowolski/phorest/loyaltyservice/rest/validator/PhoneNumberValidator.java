package pl.maciejdobrowolski.phorest.loyaltyservice.rest.validator;

import pl.maciejdobrowolski.phorest.loyaltyservice.domain.PhoneNumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneNumberValidator implements ConstraintValidator<ValidPhoneNumber, String> {
    @Override
    public void initialize(ValidPhoneNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try {
            // for now, just mocked it with a business object validation
            PhoneNumber.from(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
