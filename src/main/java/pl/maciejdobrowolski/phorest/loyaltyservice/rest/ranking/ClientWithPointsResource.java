package pl.maciejdobrowolski.phorest.loyaltyservice.rest.ranking;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

@Data
@Relation(value = "ranked-client", collectionRelation = "ranked-client")
@EqualsAndHashCode(callSuper = false)
public class ClientWithPointsResource extends ResourceSupport {

    String firstName;
    String lastName;
    long totalPoints;

}
