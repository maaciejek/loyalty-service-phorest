package pl.maciejdobrowolski.phorest.loyaltyservice.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.maciejdobrowolski.phorest.loyaltyservice.business.ClientHasAppointmentsException;
import pl.maciejdobrowolski.phorest.loyaltyservice.business.ClientNotFoundException;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.InvalidDataException;
import pl.maciejdobrowolski.phorest.loyaltyservice.importing.CsvParsingException;

@RestControllerAdvice
public class ExceptionAdvisor {

    @ExceptionHandler
    public ResponseEntity handleInvalidInput(InvalidDataException e) {
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler
    public ResponseEntity handleClientNotFound(ClientNotFoundException e) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler
    public ResponseEntity handleCsvParseError(CsvParsingException e) {
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler
    public ResponseEntity handleClientHasAppointmentsException(ClientHasAppointmentsException e) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
    }

}
