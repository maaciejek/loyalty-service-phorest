package pl.maciejdobrowolski.phorest.loyaltyservice.rest.client;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Client;

@Component
public class ClientResourceAssembler extends ResourceAssemblerSupport<Client, ClientResource> {

    public ClientResourceAssembler() {
        super(ClientController.class, ClientResource.class);
    }

    @Override
    public ClientResource toResource(Client client) {
        ClientResource clientResource = createResourceWithId(client.getId().asString(), client);
        clientResource.setFirstName(client.getFirstName());
        clientResource.setLastName(client.getLastName());
        clientResource.setBanned(client.isBanned());
        clientResource.setEmail(client.getEmail().toString());
        clientResource.setPhone(client.getPhone().toString());
        clientResource.setGender(client.getGender());
        return clientResource;
    }


}
