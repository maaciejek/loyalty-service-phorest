package pl.maciejdobrowolski.phorest.loyaltyservice.rest.client;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Gender;
import pl.maciejdobrowolski.phorest.loyaltyservice.rest.validator.ValidPhoneNumber;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class UpdateClientRequest {

    @NotBlank
    @Length(max = 255)
    String firstName;

    @NotBlank
    @Length(max = 255)
    String lastName;

    @Email
    @NotBlank
    @Length(max = 255)
    String email;

    @ValidPhoneNumber
    @NotBlank
    @Length(max = 20)
    String phone;

    @NotNull
    Gender gender;

    boolean banned;
}
