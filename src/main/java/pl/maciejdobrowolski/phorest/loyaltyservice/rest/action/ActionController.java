package pl.maciejdobrowolski.phorest.loyaltyservice.rest.action;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Action;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ActionId;
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.ActionRepository;

@RestController
@RequestMapping(path = "/api/actions")
public class ActionController {

    private final ActionRepository repository;
    private final ActionResourceAssembler resourceAssembler;
    private final PagedResourcesAssembler<Action> pageAssembler;

    public ActionController(ActionRepository repository,
                            ActionResourceAssembler resourceAssembler,
                            PagedResourcesAssembler<Action> pageAssembler) {
        this.repository = repository;
        this.resourceAssembler = resourceAssembler;
        this.pageAssembler = pageAssembler;
    }


    @GetMapping("/{id}")
    public ResponseEntity<ActionResource> findOne(@PathVariable("id") String id) {
        return repository.findById(ActionId.from(id))
                .map(resourceAssembler::toResource)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public Resources<ActionResource> findAll(@PageableDefault(size = 20) Pageable pageable) {
        Page<Action> page = repository.findAll(pageable);
        return pageAssembler.toResource(page, resourceAssembler);
    }

}
