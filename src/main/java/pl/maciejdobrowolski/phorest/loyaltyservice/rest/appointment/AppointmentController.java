package pl.maciejdobrowolski.phorest.loyaltyservice.rest.appointment;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Appointment;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.AppointmentId;
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.AppointmentRepository;

@RestController
@RequestMapping(path = "/api/appointments")
public class AppointmentController {

    private final AppointmentRepository repository;
    private final AppointmentResourceAssembler resourceAssembler;
    private final PagedResourcesAssembler<Appointment> pageAssembler;

    public AppointmentController(AppointmentRepository repository,
                                 AppointmentResourceAssembler resourceAssembler,
                                 PagedResourcesAssembler<Appointment> pageAssembler) {
        this.repository = repository;
        this.resourceAssembler = resourceAssembler;
        this.pageAssembler = pageAssembler;
    }


    @GetMapping("/{id}")
    public ResponseEntity<AppointmentResource> findOne(@PathVariable("id") String id) {
        return repository.findById(AppointmentId.from(id))
                .map(resourceAssembler::toResource)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public Resources<AppointmentResource> findAll(@PageableDefault(size = 20) Pageable pageable) {
        Page<Appointment> page = repository.findAll(pageable);
        return pageAssembler.toResource(page, resourceAssembler);
    }

}
