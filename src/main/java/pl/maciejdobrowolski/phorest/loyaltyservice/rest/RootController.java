package pl.maciejdobrowolski.phorest.loyaltyservice.rest;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.maciejdobrowolski.phorest.loyaltyservice.rest.action.ActionController;
import pl.maciejdobrowolski.phorest.loyaltyservice.rest.appointment.AppointmentController;
import pl.maciejdobrowolski.phorest.loyaltyservice.rest.client.ClientController;
import pl.maciejdobrowolski.phorest.loyaltyservice.rest.ranking.RankingController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "/api")
public class RootController {

    @GetMapping
    public ResourceSupport root() {
        ResourceSupport resource = new ResourceSupport();
        resource.add(linkTo(methodOn(ClientController.class).findAll(null)).withRel("clients"));
        resource.add(linkTo(methodOn(AppointmentController.class).findAll(null)).withRel("appointments"));
        resource.add(linkTo(methodOn(ActionController.class).findAll(null)).withRel("actions"));
        resource.add(linkTo(methodOn(RankingController.class).rankingRoot()).withRel("ranking"));
        return resource;
    }

}
