package pl.maciejdobrowolski.phorest.loyaltyservice.rest.imports;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.maciejdobrowolski.phorest.loyaltyservice.importing.AppointmentCsvImporter;

import java.io.IOException;

@RestController
@RequestMapping(path = "/api/import/appointments")
@Slf4j
public class AppointmentImportController {

    private final AppointmentCsvImporter csvImporter;

    public AppointmentImportController(AppointmentCsvImporter csvImporter) {
        this.csvImporter = csvImporter;
    }

    @PostMapping(consumes = {CsvMediaTypes.APP_CSV, CsvMediaTypes.TEXT_CSV})
    public ResponseEntity<?> importAppointments(@RequestBody Resource stream) {
        try {
            csvImporter.importAppointments(stream.getInputStream());
            return ResponseEntity.noContent().build();
        } catch (IOException e) {
            log.error("There is a problem with reading csv input stream", e);
            return ResponseEntity.badRequest().build();
        }
    }

}
