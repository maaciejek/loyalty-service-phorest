package pl.maciejdobrowolski.phorest.loyaltyservice.rest.ranking;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.projection.ClientWithPoints;
import pl.maciejdobrowolski.phorest.loyaltyservice.rest.client.ClientController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ClientWithPointsResourceAssembler extends ResourceAssemblerSupport<ClientWithPoints, ClientWithPointsResource> {

    public ClientWithPointsResourceAssembler() {
        super(RankingController.class, ClientWithPointsResource.class);
    }

    @Override
    public ClientWithPointsResource toResource(ClientWithPoints client) {
        ClientWithPointsResource resource = instantiateResource(client);
        resource.setFirstName(client.getFirstName());
        resource.setLastName(client.getLastName());
        resource.setTotalPoints(client.getTotalPoints());
        resource.add(linkTo(methodOn(ClientController.class).find(client.getClientId())).withRel("client"));
        return resource;
    }

}
