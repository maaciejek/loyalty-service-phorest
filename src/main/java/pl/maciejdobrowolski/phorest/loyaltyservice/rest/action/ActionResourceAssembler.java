package pl.maciejdobrowolski.phorest.loyaltyservice.rest.action;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Action;
import pl.maciejdobrowolski.phorest.loyaltyservice.rest.appointment.AppointmentController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ActionResourceAssembler extends ResourceAssemblerSupport<Action, ActionResource> {

    public ActionResourceAssembler() {
        super(ActionController.class, ActionResource.class);
    }

    @Override
    public ActionResource toResource(Action action) {
        ActionResource actionResource = createResourceWithId(action.getId().asString(), action);
        actionResource.setLoyaltyPoints(action.getLoyaltyPoints());
        actionResource.setName(action.getName());
        actionResource.setPrice(action.getPrice());
        actionResource.setType(action.getType());
        actionResource.add(linkTo(methodOn(AppointmentController.class).findOne(action.getAppointmentId().asString())).withRel("appointment"));
        return actionResource;
    }


}
