package pl.maciejdobrowolski.phorest.loyaltyservice.business.command;

import lombok.Builder;
import lombok.Value;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ClientId;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Email;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Gender;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.PhoneNumber;

/**
 * Wrapper object (not really a command) used to create a new client
 *
 * @see pl.maciejdobrowolski.phorest.loyaltyservice.business.ClientService
 */
@Builder
@Value
public class ClientCreateCommand {
    ClientId id;
    String firstName;
    String lastName;
    Email email;
    PhoneNumber phone;
    Gender gender;
    boolean banned;
}
