package pl.maciejdobrowolski.phorest.loyaltyservice.business.command;

import lombok.Builder;
import lombok.Value;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ClientId;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Email;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Gender;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.PhoneNumber;

/**
 * Wrapper object used to update a client
 *
 * @see pl.maciejdobrowolski.phorest.loyaltyservice.business.ClientService
 */
@Builder
@Value
public class ClientUpdateCommand {
    ClientId id;
    String firstName;
    String lastName;
    Email email;
    PhoneNumber phone;
    Gender gender;
    boolean banned;
}
