package pl.maciejdobrowolski.phorest.loyaltyservice.business;

import static java.lang.String.format;

public class ClientNotFoundException extends RuntimeException {

    ClientNotFoundException(String clientId) {
        super(format("Client %s not found!", clientId));
    }
}
