package pl.maciejdobrowolski.phorest.loyaltyservice.business;

import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ClientId;

import static java.lang.String.format;

/**
 * Exception thrown when Client is to be removed, but it still has some appointments
 */
public class ClientHasAppointmentsException extends RuntimeException {
    public ClientHasAppointmentsException(ClientId clientId) {
        super(format("Client %s has existing appointments", clientId));
    }
}
