package pl.maciejdobrowolski.phorest.loyaltyservice.business;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.maciejdobrowolski.phorest.loyaltyservice.business.command.ClientCreateCommand;
import pl.maciejdobrowolski.phorest.loyaltyservice.business.command.ClientUpdateCommand;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Client;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ClientId;
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.ClientRepository;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

/**
 * General facade-service for handling Client
 */
@Service
@Slf4j
public class ClientService {

    private final ClientRepository repository;

    public ClientService(ClientRepository repository) {
        this.repository = repository;
    }


    public Optional<Client> findById(ClientId clientId) {
        return repository.findById(clientId);
    }

    @Transactional
    public void update(ClientUpdateCommand updateCommand) {
        log.debug("Updating client {} with {}", updateCommand.getId(), updateCommand);
        ClientId clientId = updateCommand.getId();
        Client client = repository.findById(clientId).orElseThrow(() -> new ClientNotFoundException(clientId.asString()));
        client.setEmail(updateCommand.getEmail());
        client.setBanned(updateCommand.isBanned());
        client.setFirstName(updateCommand.getFirstName());
        client.setLastName(updateCommand.getLastName());
        client.setGender(updateCommand.getGender());
        client.setPhone(updateCommand.getPhone());
    }

    public void delete(ClientId clientId) {
        try {
            // doing this check by appointmentRepository.existsByClientId() would still give a chance
            // to hit an exception an a DB, this way is dirty but we have fully covered case
            repository.deleteById(clientId);
        } catch (DataIntegrityViolationException e) {
            throw new ClientHasAppointmentsException(clientId);
        }
    }

    public Page<Client> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public ClientId create(ClientCreateCommand createCommand) {
        ClientId id = clientId(createCommand);
        log.debug("Creating new client ({}) with {}", id, createCommand);
        Client client = new Client();
        client.setId(id);
        client.setBanned(createCommand.isBanned());
        client.setEmail(createCommand.getEmail());
        client.setFirstName(createCommand.getFirstName());
        client.setLastName(createCommand.getLastName());
        client.setGender(createCommand.getGender());
        client.setPhone(createCommand.getPhone());
        return repository.save(client).getId();
    }

    private ClientId clientId(ClientCreateCommand createCommand) {
        return Optional.ofNullable(createCommand.getId())
                .orElseGet(() -> ClientId.from(UUID.randomUUID().toString()));
    }
}
