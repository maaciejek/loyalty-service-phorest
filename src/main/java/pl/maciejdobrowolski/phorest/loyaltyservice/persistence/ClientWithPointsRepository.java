package pl.maciejdobrowolski.phorest.loyaltyservice.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Client;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ClientId;
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.projection.ClientWithPoints;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
// although it is just a projection repository, it has to be a repository for some entity
@RepositoryDefinition(domainClass = Client.class, idClass = ClientId.class)
public interface ClientWithPointsRepository {

    @Query("select new pl.maciejdobrowolski.phorest.loyaltyservice.persistence.projection.ClientWithPoints(" +
            "   c.id, " +
            "   c.firstName, " +
            "   c.lastName, " +
            "   sum(act.loyaltyPoints)) " +
            "from Client c " +
            "   join c.appointments a " +
            "   join a.actions act " +
            "where c.banned = false" +
            "   and a.end > :since " +
            "group by c.id " +
            "order by sum(act.loyaltyPoints) desc")
    Page<ClientWithPoints> findAll(Pageable pageable, @Param("since") ZonedDateTime since);

    default List<ClientWithPoints> top(int topCount, ZonedDateTime since) {
        PageRequest pageRequest = PageRequest.of(0, topCount);
        return findAll(pageRequest, since).getContent();
    }


}
