package pl.maciejdobrowolski.phorest.loyaltyservice.persistence.projection;

import lombok.Value;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ClientId;

/**
 * A representation of a single ranking position
 */
@Value
public class ClientWithPoints {

    String clientId;
    String firstName;
    String lastName;
    long totalPoints;

    public ClientWithPoints(ClientId clientId, String firstName, String lastName, long totalPoints) {
        this.clientId = clientId.asString();
        this.firstName = firstName;
        this.lastName = lastName;
        this.totalPoints = totalPoints;
    }
}
