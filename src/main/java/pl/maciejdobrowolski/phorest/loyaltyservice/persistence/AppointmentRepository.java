package pl.maciejdobrowolski.phorest.loyaltyservice.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Appointment;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.AppointmentId;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, AppointmentId> {
}
