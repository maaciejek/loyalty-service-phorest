package pl.maciejdobrowolski.phorest.loyaltyservice.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Client;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ClientId;

public interface ClientRepository extends JpaRepository<Client, ClientId> {
}
