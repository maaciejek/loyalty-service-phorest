package pl.maciejdobrowolski.phorest.loyaltyservice.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Action;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ActionId;

@Repository
public interface ActionRepository extends JpaRepository<Action, ActionId> {
}
