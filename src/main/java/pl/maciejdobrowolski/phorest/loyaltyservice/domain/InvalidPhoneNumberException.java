package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

import static java.lang.String.format;

public class InvalidPhoneNumberException extends InvalidDataException {

    InvalidPhoneNumberException(String phoneNumber) {
        super(format("Incorrect phone number %s!", phoneNumber));
    }
}
