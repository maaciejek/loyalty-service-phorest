package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Optional;

@Embeddable
@EqualsAndHashCode
public class PhoneNumber {

    @Column(name = "PHONE")
    private String number;

    @Override
    public String toString() {
        return number;
    }

    PhoneNumber() {
    }

    public String asString() {
        return number;
    }

    private PhoneNumber(String number) {
        this.number = number;
    }

    public static PhoneNumber from(String originalNumber) {
        String number = sanitize(originalNumber);
        validate(number, originalNumber);
        return new PhoneNumber(number);
    }

    private static String sanitize(String number) {
        return Optional.ofNullable(number)
                .map(String::strip)
                .map(num -> num.replaceAll("[^\\d]", ""))
                .orElse("");
    }

    private static void validate(String number, String originalNumber) {
        // some really sophisticated phone number validation
        // when it gets more complex, this also should end up somewhere else
        Optional.ofNullable(number)
                .filter(num -> num.length() > 2)
                .orElseThrow(() -> new InvalidPhoneNumberException(originalNumber));

    }
}
