package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

import static java.lang.String.format;

public class InvalidEmailAddressException extends InvalidDataException {

    InvalidEmailAddressException(String email) {
        super(format("Incorrect email address %s!", email));
    }
}
