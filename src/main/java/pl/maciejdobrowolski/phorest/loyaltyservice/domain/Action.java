package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Action is a representation of an activity that brought a Client some loyalty points
 */
@Entity
@Data
@Table(name = "ACTIONS")
public class Action {

    @EmbeddedId
    private ActionId id;

    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "APPOINTMENT_ID"))
    private AppointmentId appointmentId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PRICE")
    private BigDecimal price;

    @Column(name = "POINTS")
    private long loyaltyPoints;

    @Enumerated(EnumType.STRING)
    private ActionType type;

    Action() {
    }

    public Action(ActionId id, AppointmentId appointmentId, String name, BigDecimal price, long loyaltyPoints, ActionType type) {
        this.id = id;
        this.appointmentId = appointmentId;
        this.name = name;
        this.price = price;
        this.loyaltyPoints = loyaltyPoints;
        this.type = type;
    }
}
