package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Data
@Table(name = "APPOINTMENTS")
public class Appointment {

    @EmbeddedId
    private AppointmentId id;

    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "CLIENT_ID"))
    private ClientId clientId;

    @OneToMany(mappedBy = "appointmentId")
    private List<Action> actions;

    @Column(name = "START")
    private ZonedDateTime start;

    @Column(name = "END")
    private ZonedDateTime end;

    Appointment() {
    }

    public Appointment(AppointmentId id, ClientId clientId, ZonedDateTime start, ZonedDateTime end) {
        this.id = id;
        this.clientId = clientId;
        this.start = start;
        this.end = end;
    }
}
