package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

public enum Gender {
    M,
    F
}
