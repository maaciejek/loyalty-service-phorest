package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

public abstract class InvalidDataException extends RuntimeException {

    InvalidDataException(String message) {
        super(message);
    }

}
