package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import java.io.Serializable;

@EqualsAndHashCode
@ToString
public class ActionId implements Serializable {

    @Column(name = "ID")
    private String id;

    public String asString() {
        return id;
    }

    ActionId() {
    }

    private ActionId(String id) {
        this.id = id;
    }

    public static ActionId from(String id) {
        return new ActionId(id);
    }
}
