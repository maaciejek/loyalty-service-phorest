package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.UUID;

@EqualsAndHashCode
@ToString
public class AppointmentId implements Serializable {

    @Column(name = "ID")
    private String id;

    AppointmentId() {
    }

    private AppointmentId(String id) {
        this.id = id;
    }

    public String asString() {
        return id;
    }

    public static AppointmentId from(String id) {
        return new AppointmentId(id);
    }

    public static AppointmentId random() {
        return new AppointmentId(UUID.randomUUID().toString());
    }
}
