package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

public enum ActionType {
    PURCHASE,
    SERVICE
}
