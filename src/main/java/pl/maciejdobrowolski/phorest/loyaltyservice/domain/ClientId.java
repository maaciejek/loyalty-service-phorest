package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import java.io.Serializable;

@EqualsAndHashCode
@ToString
public class ClientId implements Serializable {

    @Column(name = "ID")
    private String id;

    ClientId() {
    }

    private ClientId(String id) {
        this.id = id;
    }

    public String asString() {
        return id;
    }

    public static ClientId from(String id) {
        return new ClientId(id);
    }

}
