package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "CLIENTS")
public class Client {

    @EmbeddedId
    private ClientId id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Embedded
    private Email email;

    @Embedded
    private PhoneNumber phone;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @OneToMany(mappedBy = "clientId")
    private List<Appointment> appointments;

    private boolean banned;

    public Client() {
    }

    //todo - to remove
    public Client(ClientId id, String firstName, String lastName, Email email, PhoneNumber phone, Gender gender, boolean banned) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
        this.banned = banned;
    }


}
