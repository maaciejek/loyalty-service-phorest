package pl.maciejdobrowolski.phorest.loyaltyservice.domain;

import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@EqualsAndHashCode
public class Email {

    @Column(name = "EMAIL")
    private String email;

    @Override
    public String toString() {
        return email;
    }

    Email() {
    }

    public String asString() {
        return email;
    }

    private Email(String email) {
        this.email = email;
    }

    public static Email from(String email) {
        validate(email);
        return new Email(email);
    }

    private static void validate(String email) {
        // some sophisticated email validation - it should eventually end up somewhere else
        if (email != null && !email.contains("@")) {
            throw new InvalidEmailAddressException(email);
        }
    }
}
