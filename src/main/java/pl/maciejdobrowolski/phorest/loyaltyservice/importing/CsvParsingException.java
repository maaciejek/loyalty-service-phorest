package pl.maciejdobrowolski.phorest.loyaltyservice.importing;

public class CsvParsingException extends RuntimeException {
    CsvParsingException(String message) {
        super(message);
    }

    CsvParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
