package pl.maciejdobrowolski.phorest.loyaltyservice.importing;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.maciejdobrowolski.phorest.loyaltyservice.business.ClientService;
import pl.maciejdobrowolski.phorest.loyaltyservice.business.command.ClientCreateCommand;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ClientId;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Email;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Gender;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.PhoneNumber;

import javax.transaction.Transactional;
import java.io.InputStream;

@Component
@Slf4j
public class ClientCsvImporter {

    private final ClientService clientService;
    private final CsvReader csvReader;

    public ClientCsvImporter(ClientService clientService, CsvReader csvReader) {
        this.clientService = clientService;
        this.csvReader = csvReader;
    }

    @Transactional
    public void importClients(InputStream csvStream) {
        log.debug("Starting parsing Client CSV data");
        csvReader.read(csvStream, CsvClient.class, this::persistClient);
        log.debug("Finished reading CSV Clients");
    }

    private void persistClient(CsvClient client) {
        clientService.create(ClientCreateCommand.builder()
                .id(ClientId.from(client.id))
                .firstName(client.firstName)
                .lastName(client.lastName)
                .banned(client.banned)
                .email(Email.from(client.email))
                .phone(PhoneNumber.from(client.phone))
                .gender(toDomainGender(client))
                .build());
    }

    private static Gender toDomainGender(CsvClient client) {
        switch (client.gender) {
            case Male:
                return Gender.M;
            case Female:
                return Gender.F;
            default:
                throw new CsvParsingException("Unknown gender: " + client.gender);
        }
    }

    @Data
    static class CsvClient {

        @JsonProperty("id")
        String id;

        @JsonProperty("first_name")
        String firstName;

        @JsonProperty("last_name")
        String lastName;

        @JsonProperty("email")
        String email;

        @JsonProperty("phone")
        String phone;

        @JsonProperty("gender")
        CsvGender gender;

        @JsonProperty("banned")
        boolean banned;
    }

    enum CsvGender {
        Male,
        Female
    }


}
