package pl.maciejdobrowolski.phorest.loyaltyservice.importing;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Action;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ActionId;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ActionType;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.AppointmentId;
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.ActionRepository;

import javax.transaction.Transactional;
import java.io.InputStream;
import java.math.BigDecimal;

@Component
@Slf4j
public class ActionCsvImporter {

    private final ActionRepository actionRepository;
    private final CsvReader csvReader;

    public ActionCsvImporter(ActionRepository actionRepository, CsvReader csvReader) {
        this.actionRepository = actionRepository;
        this.csvReader = csvReader;
    }

    @Transactional
    public void importActions(InputStream csvStream, ActionType type) {
        log.debug("Starting parsing {} CSV data", type);
        csvReader.read(csvStream, CsvAction.class, csvAction -> persistAction(csvAction, type));
        log.debug("Finished reading CSV {}", type);
    }

    private void persistAction(CsvAction action, ActionType type) {
        actionRepository.save(
                new Action(
                        ActionId.from(action.id),
                        AppointmentId.from(action.appointmentId),
                        action.name,
                        action.price,
                        action.loyaltyPoints,
                        type
                )
        );
    }

    @Data
    static class CsvAction {

        @JsonProperty("id")
        String id;

        @JsonProperty("appointment_id")
        String appointmentId;

        @JsonProperty("name")
        String name;

        @JsonProperty("price")
        BigDecimal price;

        @JsonProperty("loyalty_points")
        long loyaltyPoints;
    }

}
