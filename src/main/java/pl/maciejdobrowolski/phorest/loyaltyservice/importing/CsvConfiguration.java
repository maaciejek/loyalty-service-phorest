package pl.maciejdobrowolski.phorest.loyaltyservice.importing;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CsvConfiguration {

    @Bean
    CsvMapper csvMapper() {
        CsvMapper mapper = new CsvMapper()
                .enable(CsvParser.Feature.TRIM_SPACES);

        return (CsvMapper) mapper
                .findAndRegisterModules();
    }

}