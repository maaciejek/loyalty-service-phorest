package pl.maciejdobrowolski.phorest.loyaltyservice.importing;

import java.io.InputStream;

public interface CsvReader {
    <T> void read(InputStream inputStream, Class<T> rowClass, JacksonCsvReader.Callback<T> rowCallback);
}
