package pl.maciejdobrowolski.phorest.loyaltyservice.importing;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.Appointment;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.AppointmentId;
import pl.maciejdobrowolski.phorest.loyaltyservice.domain.ClientId;
import pl.maciejdobrowolski.phorest.loyaltyservice.persistence.AppointmentRepository;

import javax.transaction.Transactional;
import java.io.InputStream;
import java.time.ZonedDateTime;

@Component
@Slf4j
public class AppointmentCsvImporter {

    private final AppointmentRepository appointmentRepository;
    private final CsvReader csvReader;

    public AppointmentCsvImporter(AppointmentRepository appointmentRepository, CsvReader csvReader) {
        this.appointmentRepository = appointmentRepository;
        this.csvReader = csvReader;
    }

    @Transactional
    public void importAppointments(InputStream csvStream) {
        log.debug("Starting parsing Appointment CSV data");
        csvReader.read(csvStream, CsvAppointment.class, this::persistAppointment);
        log.debug("Finished reading CSV Appointments");
    }

    private void persistAppointment(CsvAppointment appointment) {
        appointmentRepository.save(new Appointment(AppointmentId.from(appointment.id), ClientId.from(appointment.clientId), appointment.start, appointment.end));
    }

    @Data
    static class CsvAppointment {

        @JsonProperty("id")
        String id;

        @JsonProperty("client_id")
        String clientId;

        @JsonProperty("start_time")
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss Z")
        ZonedDateTime start;

        @JsonProperty("end_time")
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss Z")
        ZonedDateTime end;
    }

}
