package pl.maciejdobrowolski.phorest.loyaltyservice.importing;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Component
@Slf4j
class JacksonCsvReader implements CsvReader {

    private final CsvMapper mapper;

    JacksonCsvReader(CsvMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public <T> void read(InputStream inputStream, Class<T> rowClass, Callback<T> rowCallback) {
        CsvSchema schema = schemaFor(rowClass);
        ObjectReader reader = this.mapper.readerFor(rowClass).with(schema);
        try (MappingIterator<T> iterator = reader.readValues(inputStream)) {
            while (iterator.hasNext()) {
                T row = iterator.next();
                log.debug("Read CSV row: {}", row);
                rowCallback.process(row);
            }
        } catch (IOException e) {
            throw new CsvParsingException("Error when parsing provided CSV", e);
        }
    }

    private <T> CsvSchema schemaFor(Class<T> rowClass) {
        return mapper.schemaFor(rowClass)
                .withHeader()
                .withColumnReordering(true);
    }

    interface Callback<T> {
        void process(T row);
    }

}
